from django.db import models
from safedelete.models import SafeDeleteModel

STATE_DEPOSIT = 1
STATE_WITHDRAW = 2
CHOICE_STATE = (
    (STATE_DEPOSIT, 'Deposit'),
    (STATE_WITHDRAW, 'Withdraw')
)

STATUS_PENDING = 0
STATUS_APPROVED = 1
STATUS_REJECTED = 2
CHOICE_STATUS = (
    (STATUS_PENDING, 'Pending'),
    (STATUS_APPROVED, 'Approved'),
    (STATUS_REJECTED, 'Rejected')
)

SETTING_TYPE_SYSTEM = 0
SETTING_TYPE = (
    (SETTING_TYPE_SYSTEM, 'System'),
)


class BaseModel(SafeDeleteModel):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
