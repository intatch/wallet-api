from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apis.serializers import GameSerializer
from apis.utils import has_permission
from apps.models import Game, CustomerCreditAccount


class GameListAPIView(APIView):
    def get_queryset(self):
        queryset = Game.objects.all()
        filter_type = self.request.query_params.get('filterType')
        if filter_type == 'customerCreditAccount':
            game_ids = CustomerCreditAccount.objects.filter(customer_id=self.request.query_params.get('customerId')).values_list('game_id', flat=True)
            queryset = queryset.exclude(pk__in=game_ids)
        return queryset

    def get(self, request, *args, **kwargs):
        games = self.get_queryset()
        game_list = []
        for game in games:
            context = self.get_game_context(game)
            game_list.append(context)
        return Response(game_list, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        has_permission(request, 'apps.can_access_setting')

        game_serializer = GameSerializer(data=request.data)
        game_serializer.is_valid(raise_exception=True)
        game = game_serializer.save()
        context = self.get_game_context(game)
        return Response(context, status=status.HTTP_201_CREATED)

    def get_game_context(self, game):
        context = {
            'id': game.pk,
            'name': game.name,
            'convert': game.convert
        }
        return context


class GameDetailAPIView(APIView):
    def get_queryset(self):
        pk = self.kwargs.get('pk')
        queryset = Game.objects.get(pk=pk)
        return queryset

    def get(self, request, *args, **kwargs):
        has_permission(request, 'apps.can_access_setting')

        self.object = self.get_queryset()
        context = self.get_context_data()
        return Response(context, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        has_permission(request, 'apps.can_access_setting')

        game = self.get_queryset()
        game_serializer = GameSerializer(game, data=request.data, partial=True)
        game_serializer.is_valid(raise_exception=True)
        self.object = game_serializer.save()
        context = self.get_context_data()
        return Response(context, status=status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):
        has_permission(request, 'apps.can_access_setting')

        game = self.get_queryset()
        game.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_context_data(self):
        game = self.object
        context = {
            'id': game.pk,
            'name': game.name,
            'convert': game.convert
        }
        return context
