from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from apps.models import Bank


class BankListAPIView(APIView):
    def get_queryset(self):
        queryset = Bank.objects.all()
        return queryset

    def get(self, request, *args, **kwargs):
        banks = self.get_queryset()
        bank_list = []
        for bank in banks:
            bank_list.append({
                'id': bank.pk,
                'name': bank.name,
                'code': bank.code
            })
        return Response(bank_list, status=status.HTTP_200_OK)
