from rest_framework import status
from django.contrib.auth.models import Group, Permission
from rest_framework.response import Response
from rest_framework.views import APIView


class GroupListAPIView(APIView):
    def get(self, request, *args, **kwargs):
        context = []
        groups = Group.objects.all()
        for group in groups:
            context.append({
                'id': group.id,
                'name': group.name
            })
        return Response(context, status=status.HTTP_200_OK)


class PermissionListAPIView(APIView):
    filter_codename_list = [
        'can_access_report',
        'can_access_setting',
        'can_access_promotion',
        'can_access_system_account',
        'can_access_transaction_bank',
        'can_access_transaction_credit',
        'can_access_transaction_history'
    ]

    def get(self, request, *args, **kwargs):
        context = []
        permissions = Permission.objects.filter(codename__in=self.filter_codename_list)
        for permission in permissions:
            context.append({
                'id': permission.id,
                'name': permission.name,
                'codename': permission.codename
            })
        return Response(context, status=status.HTTP_200_OK)
