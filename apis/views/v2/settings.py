from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.models import Setting


class SettingAPIView(APIView):
    filter_list = [
        'line_id',
        'line_qr_code_url'
    ]

    def get_queryset(self):
        queryset = Setting.objects.filter(key__in=self.filter_list)
        return queryset

    def get(self, request, *args, **kwargs):
        context = {}
        system_settings = self.get_queryset()
        for system_setting in system_settings:
            context[system_setting.key] = system_setting.value
        return Response(context, status=status.HTTP_200_OK)
