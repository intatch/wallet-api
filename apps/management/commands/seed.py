from django.core.management import BaseCommand

from apps.management.commands.seeders import seeder_banks
from apps.models import User, Setting, Bank, CustomerLevel


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.user_seeder()
        self.bank_seeder()
        self.setting_seeder()
        self.customer_level_seeder()

    def user_seeder(self):
        if User.objects.filter(username='superadmin').exists() is False:
            User.objects.create_superuser(
                email='superadmin@zyncsolution.com',
                username='superadmin',
                password='1234',
                first_name='Superadmin',
                last_name='Zyncsolution'
            )

    def bank_seeder(self):
        for bank in seeder_banks.dataset:
            if Bank.objects.filter(code=bank['code']).exists() is False:
                Bank.objects.create(code=bank['code'], name=bank['name'])

    def customer_level_seeder(self):
        if CustomerLevel.objects.filter(is_default=True).exists() is False:
            CustomerLevel.objects.create(name='ผู้ใช้ทั่วไป', is_default=True)

    def setting_seeder(self):
        if Setting.objects.filter(key='ignore_token').exists() is False:
            Setting.objects.create(key='ignore_token', value='1234')

        if Setting.objects.filter(key='line_qr_code_url').exists() is False:
            Setting.objects.create(key='line_qr_code_url', value='#')

        if Setting.objects.filter(key='line_id').exists() is False:
            Setting.objects.create(key='line_id', value='-')
