from django.contrib import admin

from django.contrib.auth.admin import UserAdmin
from django.urls import reverse
from django.utils.html import format_html
from django_otp.plugins.otp_totp.admin import TOTPDeviceAdmin
from django_otp.plugins.otp_totp.models import TOTPDevice

from apps import models


class CustomTOTPDeviceAdmin(TOTPDeviceAdmin):
    list_filter = ['confirmed']
    list_display = ['user', 'name', 'confirmed', 'qrcode_link']
    search_fields = ['name', 'user__username']

    def qrcode_link(self, device):
        try:
            href = reverse('admin:otp_totp_totpdevice_config', kwargs={'pk': device.pk})
            link = format_html('<a href="{}" target="_blank">QR Code</a>', href)
        except Exception:
            link = ''
        return link


class BankAdmin(admin.ModelAdmin):
    list_display = ['code', 'name']
    search_fields = ['code', 'name']


class GameAdmin(admin.ModelAdmin):
    list_display = ['name']
    search_fields = ['name']


# class CustomerAdmin(admin.ModelAdmin):
#     list_filter = ['bank', 'is_secret']
#     list_display = ['username', 'name', 'telephone', 'bank_no', 'bank', 'is_secret', 'secret_code']
#     search_fields = ['username', 'name']
#
#
# class TransactionAdmin(admin.ModelAdmin):
#     list_filter = ['is_new_customer', 'state', 'status']
#     list_display = ['id', 'customer', 'amount', 'state', 'status', 'is_new_customer']
#     search_fields = ['id', 'customer']
#
#
# class BankTransactionAdmin(admin.ModelAdmin):
#     list_filter = ['status']
#     list_display = ['id', 'transaction', 'amount', 'transaction_at', 'status']
#     search_fields = ['id', 'transaction']
#
#
# class TopUpTransactionAdmin(admin.ModelAdmin):
#     list_filter = ['status']
#     list_display = ['id', 'transaction', 'credit', 'bonus_credit', 'total_credit', 'status']
#     search_fields = ['id', 'transaction']
#
#
# class SystemAccountAdmin(admin.ModelAdmin):
#     list_filter = ['account_type', 'is_active']
#     list_display = ['name', 'account_no', 'balance', 'account_type', 'is_active']
#     search_fields = ['name', 'account_no']


class SettingAdmin(admin.ModelAdmin):
    fields = ['key', 'value', 'setting_type']
    list_display = ['key', 'value', 'setting_type']
    readonly_fields = ['key']

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_actions(self, request):
        actions = super(SettingAdmin, self).get_actions(request)
        actions.pop('delete_selected', None)
        return actions


admin.site.unregister(TOTPDevice)
admin.site.register(TOTPDevice, CustomTOTPDeviceAdmin)

admin.site.register(models.User, UserAdmin)
admin.site.register(models.Bank, BankAdmin)
admin.site.register(models.Game, GameAdmin)
# admin.site.register(models.Transaction, TransactionAdmin)
# admin.site.register(models.TransactionCredit, TopUpTransactionAdmin)
# admin.site.register(models.TransactionBank, BankTransactionAdmin)
# admin.site.register(models.SystemBankAccount, SystemAccountAdmin)
admin.site.register(models.Setting, SettingAdmin)
